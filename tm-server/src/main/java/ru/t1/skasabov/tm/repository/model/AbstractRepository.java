package ru.t1.skasabov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.api.repository.model.IRepository;
import ru.t1.skasabov.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final EntityManager entityManager;

    protected AbstractRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void removeAll(@NotNull final Collection<M> models) {
        for (M model : models) {
            removeOne(model);
        }
    }

    @NotNull
    @Override
    public Collection<M> addAll(@NotNull final Collection<M> models) {
        @NotNull final List<M> result = new ArrayList<>();
        for (M model : models) {
            add(model);
            result.add(model);
        }
        return result;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) {
        removeAll();
        return addAll(models);
    }

    @NotNull
    @Override
    public Boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

    @Override
    public void removeOne(@NotNull final M model) {
        entityManager.remove(model);
    }

}
