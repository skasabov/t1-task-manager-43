package ru.t1.skasabov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.skasabov.tm.dto.model.ProjectDTO;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectDTORepository extends AbstractUserOwnedDTORepository<ProjectDTO> implements IProjectDTORepository {

    public ProjectDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll() {
        return entityManager.createQuery("SELECT p FROM ProjectDTO p", ProjectDTO.class)
                .getResultList();
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@NotNull final String id) {
        return entityManager.createQuery("SELECT p FROM ProjectDTO p WHERE p.id = :id", ProjectDTO.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public ProjectDTO findOneByIndex(@NotNull final Integer index) {
        return entityManager.createQuery("SELECT p FROM ProjectDTO p", ProjectDTO.class)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(p) FROM ProjectDTO p", Long.class)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public void removeOneById(@NotNull final String id) {
        entityManager.createQuery("DELETE FROM ProjectDTO p WHERE p.id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void removeOneByIndex(@NotNull final Integer index) {
        entityManager.remove(findOneByIndex(index));
    }

    @Override
    public void removeAll() {
        entityManager.createQuery("DELETE FROM ProjectDTO p")
                .executeUpdate();
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll(@NotNull final String userId) {
        return entityManager.createQuery("SELECT p FROM ProjectDTO p WHERE p.userId = :userId", ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@NotNull final String userId, @NotNull final String id) {
        return entityManager.createQuery("SELECT p FROM ProjectDTO p WHERE p.userId = :userId AND p.id = :id",
                        ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public ProjectDTO findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return entityManager.createQuery("SELECT p FROM ProjectDTO p WHERE p.userId = :userId", ProjectDTO.class)
                .setParameter("userId", userId)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public long getSize(@NotNull final String userId) {
        return entityManager.createQuery("SELECT COUNT(p) FROM ProjectDTO p WHERE p.userId = :userId", Long.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public void removeOneById(@NotNull final String userId, @NotNull final String id) {
        entityManager.createQuery("DELETE FROM ProjectDTO p WHERE p.userId = :userId AND p.id = :id")
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        entityManager.remove(findOneByIndex(userId, index));
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        entityManager.createQuery("DELETE FROM ProjectDTO p WHERE p.userId = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllSortByCreated() {
        return entityManager.createQuery("SELECT p FROM ProjectDTO p ORDER BY p.created",
                        ProjectDTO.class)
                .getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllSortByStatus() {
        return entityManager.createQuery("SELECT p FROM ProjectDTO p ORDER BY p.status",
                        ProjectDTO.class)
                .getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllSortByName() {
        return entityManager.createQuery("SELECT p FROM ProjectDTO p ORDER BY p.name",
                        ProjectDTO.class)
                .getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllSortByCreatedForUser(@NotNull final String userId) {
        return entityManager.createQuery("SELECT p FROM ProjectDTO p WHERE p.userId = :userId ORDER BY p.created",
                        ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllSortByStatusForUser(@NotNull final String userId) {
        return entityManager.createQuery("SELECT p FROM ProjectDTO p WHERE p.userId = :userId ORDER BY p.status",
                        ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllSortByNameForUser(@NotNull final String userId) {
        return entityManager.createQuery("SELECT p FROM ProjectDTO p WHERE p.userId = :userId ORDER BY p.name",
                        ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }
    
}
