package ru.t1.skasabov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.api.service.dto.*;

public interface IServiceLocator {

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IProjectDTOService getProjectService();

    @NotNull
    ITaskDTOService getTaskService();

    @NotNull
    IProjectTaskDTOService getProjectTaskService();

    @NotNull
    IUserDTOService getUserService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    ISessionDTOService getSessionService();

    @NotNull
    IConnectionService getConnectionService();

}
