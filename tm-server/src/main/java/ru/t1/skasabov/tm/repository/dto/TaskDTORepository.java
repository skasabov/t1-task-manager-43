package ru.t1.skasabov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.skasabov.tm.dto.model.TaskDTO;

import javax.persistence.EntityManager;
import java.util.List;

public final class TaskDTORepository extends AbstractUserOwnedDTORepository<TaskDTO> implements ITaskDTORepository {

    public TaskDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll() {
        return entityManager.createQuery("SELECT t FROM TaskDTO t", TaskDTO.class)
                .getResultList();
    }

    @Nullable
    @Override
    public TaskDTO findOneById(@NotNull final String id) {
        return entityManager.createQuery("SELECT p FROM TaskDTO p WHERE p.id = :id", TaskDTO.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public TaskDTO findOneByIndex(@NotNull final Integer index) {
        return entityManager.createQuery("SELECT t FROM TaskDTO t", TaskDTO.class)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(t) FROM TaskDTO t", Long.class)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public void removeOneById(@NotNull final String id) {
        entityManager.createQuery("DELETE FROM TaskDTO t WHERE t.id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void removeOneByIndex(@NotNull final Integer index) {
        entityManager.remove(findOneByIndex(index));
    }

    @Override
    public void removeAll() {
        entityManager.createQuery("DELETE FROM TaskDTO t")
                .executeUpdate();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(@NotNull final String userId) {
        return entityManager.createQuery("SELECT t FROM TaskDTO t WHERE t.userId = :userId", TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public TaskDTO findOneById(@NotNull final String userId, @NotNull final String id) {
        return entityManager.createQuery("SELECT t FROM TaskDTO t WHERE t.userId = :userId AND t.id = :id",
                        TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public TaskDTO findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return entityManager.createQuery("SELECT t FROM TaskDTO t WHERE t.userId = :userId", TaskDTO.class)
                .setParameter("userId", userId)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public long getSize(@NotNull final String userId) {
        return entityManager.createQuery("SELECT COUNT(t) FROM TaskDTO t WHERE t.userId = :userId", Long.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public void removeOneById(@NotNull final String userId, @NotNull final String id) {
        entityManager.createQuery("DELETE FROM TaskDTO t WHERE t.userId = :userId AND t.id = :id")
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        entityManager.remove(findOneByIndex(userId, index));
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        entityManager.createQuery("DELETE FROM TaskDTO t WHERE t.userId = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllSortByCreated() {
        return entityManager.createQuery("SELECT t FROM TaskDTO t ORDER BY t.created",
                        TaskDTO.class)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllSortByStatus() {
        return entityManager.createQuery("SELECT t FROM TaskDTO t ORDER BY t.status",
                        TaskDTO.class)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllSortByName() {
        return entityManager.createQuery("SELECT t FROM TaskDTO t ORDER BY t.name",
                        TaskDTO.class)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllSortByCreatedForUser(@NotNull final String userId) {
        return entityManager.createQuery("SELECT t FROM TaskDTO t WHERE t.userId = :userId ORDER BY t.created",
                        TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllSortByStatusForUser(@NotNull final String userId) {
        return entityManager.createQuery("SELECT t FROM TaskDTO t WHERE t.userId = :userId ORDER BY t.status",
                        TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllSortByNameForUser(@NotNull final String userId) {
        return entityManager.createQuery("SELECT t FROM TaskDTO t WHERE t.userId = :userId ORDER BY t.name",
                        TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return entityManager.createQuery("SELECT t FROM TaskDTO t WHERE t.userId = :userId AND t.projectId = :projectId",
                        TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }
    
}
