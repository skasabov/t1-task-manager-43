package ru.t1.skasabov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    List<Task> findAllSortByCreated();

    @NotNull
    List<Task> findAllSortByStatus();

    @NotNull
    List<Task> findAllSortByName();

    @NotNull
    List<Task> findAllSortByCreatedForUser(@NotNull String userId);

    @NotNull
    List<Task> findAllSortByStatusForUser(@NotNull String userId);

    @NotNull
    List<Task> findAllSortByNameForUser(@NotNull String userId);

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}
