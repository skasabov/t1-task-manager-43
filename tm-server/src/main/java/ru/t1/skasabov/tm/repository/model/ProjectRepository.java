package ru.t1.skasabov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.api.repository.model.IProjectRepository;
import ru.t1.skasabov.tm.model.Project;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        return entityManager.createQuery("SELECT p FROM Project p", Project.class)
                .getResultList();
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull final String id) {
        return entityManager.createQuery("SELECT p FROM Project p WHERE p.id = :id", Project.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public Project findOneByIndex(@NotNull final Integer index) {
        return entityManager.createQuery("SELECT p FROM Project p", Project.class)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(p) FROM Project p", Long.class)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public void removeOneById(@NotNull final String id) {
        entityManager.createQuery("DELETE FROM Project p WHERE p.id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void removeOneByIndex(@NotNull final Integer index) {
        entityManager.remove(findOneByIndex(index));
    }

    @Override
    public void removeAll() {
        entityManager.createQuery("DELETE FROM Project p")
                .executeUpdate();
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId) {
        return entityManager.createQuery("SELECT p FROM Project p WHERE p.user.id = :userId", Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull final String userId, @NotNull final String id) {
        return entityManager.createQuery("SELECT p FROM Project p WHERE p.user.id = :userId AND p.id = :id",
                        Project.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public Project findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return entityManager.createQuery("SELECT p FROM Project p WHERE p.user.id = :userId", Project.class)
                .setParameter("userId", userId)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public long getSize(@NotNull final String userId) {
        return entityManager.createQuery("SELECT COUNT(p) FROM Project p WHERE p.user.id = :userId", Long.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public void removeOneById(@NotNull final String userId, @NotNull final String id) {
        entityManager.createQuery("DELETE FROM Project p WHERE p.user.id = :userId AND p.id = :id")
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        entityManager.remove(findOneByIndex(userId, index));
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        entityManager.createQuery("DELETE FROM Project p WHERE p.user.id = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @NotNull
    @Override
    public List<Project> findAllSortByCreated() {
        return entityManager.createQuery("SELECT p FROM Project p ORDER BY p.created",
                        Project.class)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Project> findAllSortByStatus() {
        return entityManager.createQuery("SELECT p FROM Project p ORDER BY p.status",
                Project.class)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Project> findAllSortByName() {
        return entityManager.createQuery("SELECT p FROM Project p ORDER BY p.name",
                        Project.class)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Project> findAllSortByCreatedForUser(@NotNull final String userId) {
        return entityManager.createQuery("SELECT p FROM Project p WHERE p.user.id = :userId ORDER BY p.created",
                        Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Project> findAllSortByStatusForUser(@NotNull final String userId) {
        return entityManager.createQuery("SELECT p FROM Project p WHERE p.user.id = :userId ORDER BY p.status",
                        Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Project> findAllSortByNameForUser(@NotNull final String userId) {
        return entityManager.createQuery("SELECT p FROM Project p WHERE p.user.id = :userId ORDER BY p.name",
                        Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

}
