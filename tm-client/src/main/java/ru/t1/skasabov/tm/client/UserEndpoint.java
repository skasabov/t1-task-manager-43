package ru.t1.skasabov.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.api.endpoint.IUserEndpoint;
import ru.t1.skasabov.tm.dto.request.*;
import ru.t1.skasabov.tm.dto.response.*;

@NoArgsConstructor
public final class UserEndpoint implements IUserEndpoint {

    @NotNull
    @Override
    public UserLockResponse lockUser(@NotNull final UserLockRequest request) {
        return IUserEndpoint.newInstance().lockUser(request);
    }

    @NotNull
    @Override
    public UserUnlockResponse unlockUser(@NotNull final UserUnlockRequest request) {
        return IUserEndpoint.newInstance().unlockUser(request);
    }

    @NotNull
    @Override
    public UserRemoveResponse removeUser(@NotNull final UserRemoveRequest request) {
        return IUserEndpoint.newInstance().removeUser(request);
    }

    @NotNull
    @Override
    public UserUpdateProfileResponse updateUserProfile(@NotNull final UserUpdateProfileRequest request) {
        return IUserEndpoint.newInstance().updateUserProfile(request);
    }

    @NotNull
    @Override
    public UserChangePasswordResponse changeUserPassword(@NotNull final UserChangePasswordRequest request) {
        return IUserEndpoint.newInstance().changeUserPassword(request);
    }

    @NotNull
    @Override
    public UserRegistryResponse registryUser(@NotNull final UserRegistryRequest request) {
        return IUserEndpoint.newInstance().registryUser(request);
    }

}
